import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { NewGameComponent } from './new-game/new-game.component';
import { NotFoundComponent } from './not-found.component';
import { GamePlatformComponent } from './game-platform/game-platform.component';
import { GameComponent } from './game/game.component';
import { GameInfoComponent } from './game-platform/game-info/game-info.component';

const routes: Routes = [
  {
    path: '', component: HomeComponent, children: [
      {path: '', component: GameComponent},
      {path: 'new/game', component: NewGameComponent},
      {path: ':platform', component: GamePlatformComponent},
      {path: ':platform/:gameName', component: GameInfoComponent},
    ]
  },
  {path: '**', component: NotFoundComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}

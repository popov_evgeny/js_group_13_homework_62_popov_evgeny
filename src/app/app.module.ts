import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { GameService } from './shared/game.service';
import { GameComponent } from './game/game.component';
import { GameItemComponent } from './game/game-item/game-item.component';
import { NewGameComponent } from './new-game/new-game.component';
import { FormsModule } from '@angular/forms';
import { HomeComponent } from './home/home.component';
import { ToolbarComponent } from './ui/toolbar/toolbar.component';
import { FooterComponent } from './ui/footer/footer.component';
import { NotFoundComponent } from './not-found.component';
import { GamePlatformComponent } from './game-platform/game-platform.component';
import { GameInfoComponent } from './game-platform/game-info/game-info.component';
import { GamesComponent } from './game-platform/games/games.component';

@NgModule({
  declarations: [
    AppComponent,
    GameComponent,
    GameItemComponent,
    NewGameComponent,
    HomeComponent,
    ToolbarComponent,
    FooterComponent,
    NotFoundComponent,
    GamePlatformComponent,
    GameInfoComponent,
    GamesComponent

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
  ],
  providers: [GameService],
  bootstrap: [AppComponent]
})
export class AppModule {

}

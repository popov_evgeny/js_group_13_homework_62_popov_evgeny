import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { Game } from '../../shared/game.model';
import { GameService } from '../../shared/game.service';


@Component({
  selector: 'app-game-info',
  templateUrl: './game-info.component.html',
  styleUrls: ['./game-info.component.css']
})
export class GameInfoComponent implements OnInit {
  game!: Game;
  arrayGamesInPlatform: Game[] = []

  constructor(private route: ActivatedRoute, private gameService: GameService) {
  }

  ngOnInit(): void {
    this.route.params.subscribe((params: Params) => {
      const gameName = params['gameName'];
      this.arrayGamesInPlatform = this.gameService.arrayGamesInPlatform;
      this.arrayGamesInPlatform.forEach((game) => {
        if (game.name === gameName){
          this.game = game;
        }
      })
    });
  }
}

import { Component, OnInit } from '@angular/core';
import { Game } from '../shared/game.model';
import { GameService } from '../shared/game.service';
import { ActivatedRoute, Params } from '@angular/router';

@Component({
  selector: 'app-game-platform',
  templateUrl: './game-platform.component.html',
  styleUrls: ['./game-platform.component.css']
})
export class GamePlatformComponent implements OnInit {
  platform = '';
  arrayGamesInPlatform: Game[] = [];

  constructor(private gameService: GameService, private route: ActivatedRoute) {
  }

  ngOnInit(): void {
    this.route.params.subscribe((params: Params) => {
      this.platform = params['platform'];
      this.getGamesInPlatform();
    });
    this.gameService.changeArrayGame.subscribe(() => {
      this.arrayGamesInPlatform = this.gameService.getGamesByPlatform(this.platform);
    });
  }

  getGamesInPlatform() {
    this.gameService.arrayGamesInPlatform = [];
    this.arrayGamesInPlatform = this.gameService.getGamesByPlatform(this.platform);
  }
}

import { Component, Input } from '@angular/core';
import { Game } from '../../shared/game.model';

@Component({
  selector: 'app-games',
  templateUrl: './games.component.html',
  styleUrls: ['./games.component.css']
})
export class GamesComponent {
  @Input() game!: Game;
}

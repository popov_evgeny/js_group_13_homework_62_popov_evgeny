import { Component, OnInit } from '@angular/core';
import { GameService } from '../shared/game.service';
import { Game } from '../shared/game.model';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  games!: Game[];
  arrayPlatforms: string[] = [];

  constructor(private gameService: GameService) {}

  ngOnInit(): void {
    this.games = this.gameService.getGames();
    this.gameService.changeArrayGame.subscribe((games: Game[]) => {
      this.games = games;
      this.getGamesPlatforms();
    });
    this.getGamesPlatforms();
  }

  getGamesPlatforms() {
    this.games.forEach((game: Game) => {
      let empty = this.arrayPlatforms.some(elem => elem === game.platform);
      if (!empty) {
        this.arrayPlatforms.push(game.platform)
      }
    });
  }
}

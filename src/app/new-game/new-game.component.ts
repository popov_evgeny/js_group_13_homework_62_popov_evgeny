import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { Game } from '../shared/game.model';
import { GameService } from '../shared/game.service';

@Component({
  selector: 'app-new-game',
  templateUrl: './new-game.component.html',
  styleUrls: ['./new-game.component.css']
})
export class NewGameComponent implements OnInit {
  games!: Game[];
  arrayPlatforms: string[] = [];
  addText = '';

  @ViewChild('nameInput') nameInput!: ElementRef;
  @ViewChild('imageUrlInput') imageUrlInput!: ElementRef;
  @ViewChild('platformInput') platformInput!: ElementRef;
  @ViewChild('descriptionInput') descriptionInput!: ElementRef;

  constructor(private gameService: GameService) {
  }

  ngOnInit(): void {
    this.games = this.gameService.getGames();
    this.gameService.changeArrayGame.subscribe((games: Game[]) => {
      this.games = games;
      this.getGamesPlatforms();
    });
    this.getGamesPlatforms();
  }

  getGamesPlatforms() {
    this.games.forEach((game: Game) => {
      let empty = this.arrayPlatforms.some(elem => elem === game.platform);
      if (!empty) {
        this.arrayPlatforms.push(game.platform)
      }
    });
  }

  creatNewGame() {
    let name = '';
    let imageUrl = '';
    let platform = '';
    let description = '';
    if (this.platformInput.nativeElement.value === ''
      || this.platformInput.nativeElement.value === 'Select platform'
      || this.nameInput.nativeElement.value === ''
      || this.imageUrlInput.nativeElement.value === ''
      || this.descriptionInput.nativeElement.value === '') {
      alert('Введите все данные пожалуйста');
    } else {
      name = this.nameInput.nativeElement.value;
      imageUrl = this.imageUrlInput.nativeElement.value;
      platform = this.platformInput.nativeElement.value;
      description = this.descriptionInput.nativeElement.value;
      const game = new Game(name, imageUrl, platform, description,);
      this.gameService.addGame(game);
      this.addText = 'Игра добавлена!'
      this.nameInput.nativeElement.value = '';
      this.imageUrlInput.nativeElement.value = '';
      this.platformInput.nativeElement.value = '';
      this.descriptionInput.nativeElement.value = '';
    }
  }
}

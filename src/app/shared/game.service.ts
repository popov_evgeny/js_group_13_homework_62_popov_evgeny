import { Game } from './game.model';
import { EventEmitter } from '@angular/core';

export class GameService {
  changeArrayGame = new EventEmitter<Game[]>();
  arrayGamesInPlatform: Game[] = [];

  private arrayGames: Game[] = [
      new Game('DOOM', 'https://disgustingmen.com/wp-content/uploads/2016/03/doomcoverart.0.jpg', 'Android', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Inventore ipsum minima sunt! Iste odio placeat quisquam quod voluptatem? Consectetur consequatur consequuntur culpa cupiditate delectus dolores incidunt inventore modi necessitatibus quia quisquam quod, recusandae repellendus rerum temporibus unde vero voluptate voluptates.'),
      new Game('GRID', 'https://www.mobygames.com/images/covers/l/597923-grid-xbox-one-front-cover.jpg', 'Android', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Inventore ipsum minima sunt! Iste odio placeat quisquam quod voluptatem? Consectetur consequatur consequuntur culpa cupiditate delectus dolores incidunt inventore modi necessitatibus quia quisquam quod, recusandae repellendus rerum temporibus unde vero voluptate voluptates.'),
      new Game('Counter-Strike', 'https://s1.gaming-cdn.com/images/products/9459/orig/counter-strike-global-offensive-pc-mac-game-steam-cover.jpg', 'IOS', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Inventore ipsum minima sunt! Iste odio placeat quisquam quod voluptatem? Consectetur consequatur consequuntur culpa cupiditate delectus dolores incidunt inventore modi necessitatibus quia quisquam quod, recusandae repellendus rerum temporibus unde vero voluptate voluptates.'),
      new Game('Battlegrounds', 'https://upload.wikimedia.org/wikipedia/ru/c/c9/%D0%9B%D0%BE%D0%B3%D0%BE%D1%82%D0%B8%D0%BF_%D0%B8%D0%B3%D1%80%D1%8B_PlayerUnknown%27s_Battlegrounds.jpg', 'IOS', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Inventore ipsum minima sunt! Iste odio placeat quisquam quod voluptatem? Consectetur consequatur consequuntur culpa cupiditate delectus dolores incidunt inventore modi necessitatibus quia quisquam quod, recusandae repellendus rerum temporibus unde vero voluptate voluptates.'),
      new Game('Assassin`s', 'https://vgtimes.ru/uploads/posts/2012-08/1344514569_ebzg9.jpg', 'Playstation', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Inventore ipsum minima sunt! Iste odio placeat quisquam quod voluptatem? Consectetur consequatur consequuntur culpa cupiditate delectus dolores incidunt inventore modi necessitatibus quia quisquam quod, recusandae repellendus rerum temporibus unde vero voluptate voluptates.'),
      new Game('Ghostrunner', 'https://cdn1.epicgames.com/95d0b9561be1464cb43bd029e94cf526/offer/GR_Portrait_Offer_1200x1600-1200x1600-a7811e23904db375486535513d10412f.jpg', 'Playstation', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Inventore ipsum minima sunt! Iste odio placeat quisquam quod voluptatem? Consectetur consequatur consequuntur culpa cupiditate delectus dolores incidunt inventore modi necessitatibus quia quisquam quod, recusandae repellendus rerum temporibus unde vero voluptate voluptates.'),
      new Game('Call of Duty', 'https://avatars.mds.yandex.net/get-mpic/4547325/img_id8123148496778633250.jpeg/orig', 'PC', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Inventore ipsum minima sunt! Iste odio placeat quisquam quod voluptatem? Consectetur consequatur consequuntur culpa cupiditate delectus dolores incidunt inventore modi necessitatibus quia quisquam quod, recusandae repellendus rerum temporibus unde vero voluptate voluptates.'),
      new Game('Battlefield 4', 'https://img.g2a.com/1080x1080/1x1x0/battlefield-4-pc-origin-key-global/59a0526c5bafe3571d1b4023', 'PC', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Inventore ipsum minima sunt! Iste odio placeat quisquam quod voluptatem? Consectetur consequatur consequuntur culpa cupiditate delectus dolores incidunt inventore modi necessitatibus quia quisquam quod, recusandae repellendus rerum temporibus unde vero voluptate voluptates.'),
  ];

  getGames() {
    return this.arrayGames.slice();
  }

  getGamesByPlatform(platform: string) {
    this.arrayGames.forEach(game => {
      if (game.platform === platform) {
        this.arrayGamesInPlatform.push(game);
      }
    });
    return this.arrayGamesInPlatform;
  }

  addGame(game: Game) {
    this.arrayGames.push(game);
    this.changeArrayGame.emit(this.arrayGames);
  }
}
